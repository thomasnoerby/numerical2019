#include<assert.h>
#include<stdio.h>
double linterp(int n, double* x, double* y, double z) {
	assert(n > 1 && z >= x[0] && z <= x[n-1]);
	
	int i = 0, j = n-1;
	while(j-i>1) {
		int mid = (j+i)/2;
		if(z>x[mid]) {i=mid;}
		else {j = mid;}
	}

	double delta_x = x[j]-x[i];
	double delta_y = y[j]-y[i];
	double p = delta_y/delta_x;

	double spline = y[i]+p*(z-x[i]);

	return spline;
}

double linterp_integ(int n, double* x, double* y, double z) {
	assert(n > 1 && z >= x[0] && z <= x[n-1]);

	int k = 0, j = n-1;
	while(j-k>1) {
		int m = (j+k)/2;
		if(z>x[m]) {k=m;}
		else {j = m;}
	}

	double sum = 0;
	for(int i = 0; i<j ; i++){
		double delta_y = y[i+1]-y[i];
		double delta_x = x[i+1]-x[i];
		double p_i = delta_y/delta_x;
		if(i<k) { 
			double integral = y[i]*(x[i+1]-x[i])+
				0.5*p_i*(x[i+1]*x[i+1]-x[i]*x[i])-p_i*x[i]*(x[i+1]-x[i]);
			sum += integral;
		}
		else {
			double rest = y[k]*(z-x[k])+0.5*p_i*(z*z-x[k]*x[k])-p_i*x[k]*(z-x[k]);
			sum += rest;
		}
	}

	return sum;
}
