#include<stdio.h>
#include<math.h>
#include"linterp.h"

int main() {
	int n = 6;
	double x[n], y[n], g[n];
	for(int i=0; i<n; i++) {
		x[i] = i;
		y[i] = x[i]*x[i];
		g[i] = 3*(1+cos(x[i]));
	}
	for(double z = 0; z<=x[n-1]; z+=0.1) {	
		double spline_y = linterp(n,x,y,z);
		double spline_g = linterp(n,x,g,z);
		printf("%g %g %g\n", z, spline_y, spline_g);
	}

	double z = 4.5;	
	double sum = linterp_integ(n, x, y, z); 
	printf("The integral from %g to %g is = %g\n",x[0],z,sum);
	
	return 0;
}
